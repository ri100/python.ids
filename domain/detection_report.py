class DetectionReport:

    def __init__(self, frame, positive_frames, all_frames, speed, time_in_miliseconds):
        self.time_in_miliseconds = time_in_miliseconds
        self.frame = frame
        self.speed = speed
        self.positive_frames = positive_frames
        self.all_frames = all_frames

    def ratio(self):
        return self.positive_frames / self.all_frames

    def __str__(self):
        return 'speed:%d, hits:%d, frames:%d time:%d' % (self.speed, self.positive_frames, self.all_frames,
                                                         self.time_in_miliseconds)
