from collections import deque

from cv2 import cv2


class Tracker:

    def __init__(self, image, object_position: tuple, trace_length=5):
        self.trk = cv2.TrackerKCF_create()
        self.trk.init(image, object_position)
        self.path = deque(maxlen=trace_length)

    def _is_path_ready(self):
        return len(self.path) == self.path.maxlen

    def append_path(self, position: tuple):
        (x1, y1, w, h) = position
        self.path.append((int(x1 + w / 2), int(y1 + h / 2)))

    def get_path(self):
        return self.path

    def is_moving(self):
        if self._is_path_ready():
            (x, y) = self.path[0]
            (x1, y1) = [sum(p) / len(p) for p in zip(*self.path)]
            # print(abs(x1 - x), abs(y1 - y))
            is_moving = abs(x1 - x) > 1.7 or abs(y1 - y) > 1.7
            return is_moving

        return None

    def update_position(self, image) -> tuple:
        return self.trk.update(image)
