from pathlib import Path


class TimePointer:

    def __init__(self, file):
        self.file = file

    def read(self):
        time_file = Path(self.file)
        if time_file.is_file():
            with open(self.file) as f:
                content = f.readline()

            return content.strip('\n')
        else:
            return '1999-01-01 00:00:00'

    def write(self, time):
        file = open(self.file, "w")
        file.write(time)
        file.close()
