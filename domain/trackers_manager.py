class TrackersManager:

    def __init__(self):
        self.trackers = []

    def add(self, tracker):
        self.trackers.append(tracker)

    def has_trackers(self):
        return len(self.trackers) > 0

    def clear(self):
        self.trackers = []

    def get(self):
        return self.trackers.copy()

    def remove(self, number):
        del self.trackers[number]

    def has_tracker_number(self, number:  int):
        return number < len(self.trackers)
