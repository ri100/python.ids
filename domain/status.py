import json
from pathlib import Path

from service.positioner import position_to_original_image


class Status:

    def __init__(self, fld, name):
        self.path = fld + '/.'+name
        self.file = None

    def close(self):
        if self.file is not None:
            self.file.close()

    def open(self):
        self.file = open(self.path, "w")

    def exists(self):
        return Path(self.path).is_file()

    def read(self):
        return open(self.path).read()

    def write(self, result, width, x1, x2, y1):
        frame_number = result['frame']
        box_number = 0
        status_dict = {
            str(frame_number): []
        }
        for (xA, yA, xB, yB, class_type) in result['objects']:
            box_number += 1
            ox1, oy1, ox2, oy2 = position_to_original_image(width, x1, x2, y1, xA, yA, xB, yB)
            status_dict[str(frame_number)].append({
                'xA': int(ox1),
                'yA': int(oy1),
                'xB': int(ox2),
                'yB': int(oy2),
                'class': class_type
            })
        self.file.write(json.dumps(status_dict) + "\n")
