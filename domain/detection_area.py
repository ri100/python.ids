from domain.config import Config
from domain.zonematch import ZoneMatch
from domain.roi import ROI


class DetectionArea:

    ONLY = 1
    ONE_OF_MANY = 2

    def __init__(self, id, zone_match: ZoneMatch, roi: ROI, config: Config, algorithm, triggers: list):
        """

        :type algorithm: Must have method __detect__, and alarm-after
        """
        self.id = id
        self.algorithm = algorithm
        self.triggers = triggers
        self.config = config
        self.zone_match = zone_match
        self.roi = roi

    def match_roi(self, notes, monitor_id):
        if self.roi.monitor_id == monitor_id:
            if self.zone_match.match_type is self.ONLY:
                return notes == self.zone_match.zone_name
            elif self.zone_match.match_type is self.ONE_OF_MANY:
                return notes.find(self.zone_match.zone_name) is not -1
        return False
