class Config:
    def __init__(self, force_detection=False, enabled=True, suppress_notifications=False, alarm_after=3,
                 detection_stops_after=15, draw_rectangle=True, show_frames=False):
        self.alarm_after = alarm_after
        self.show_frames = show_frames
        self.detection_stops_after = detection_stops_after
        self.draw_rectangle = draw_rectangle
        self.suppress_notifications = suppress_notifications
        self.enabled = enabled
        self.force_detection = force_detection
