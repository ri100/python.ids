import time

from datetime import datetime


class ZoneMatch:

    def __init__(self, zone_name, match_type, period: tuple):
        """
        ZOneMinder notes
        :type zone_name: string
        :type match_type: int
        """
        self.period = period
        self.match_type = match_type
        self.zone_name = zone_name

    def is_it_time(self, now):
        (start, end) = self.period

        start_time = datetime.strptime(start, "%H:%M:%S")
        end_time = datetime.strptime(end, "%H:%M:%S")
        start = start_time.replace(year=now.year, month=now.month, day=now.day)
        end = end_time.replace(year=now.year, month=now.month, day=now.day)

        if start > end:
            return start <= now or now <= end

        return start <= now <= end


if __name__ == "__main__":
    x = ZoneMatch("", 1, ("19:00:01", "07:34:34"))
    print(x.is_it_time(datetime.strptime("2018-02-27 19:00:01", "%Y-%m-%d %H:%M:%S")))

    x = ZoneMatch("", 1, ("19:00:01", "07:34:34"))
    print(x.is_it_time(datetime.strptime("2018-02-27 07:40:00", "%Y-%m-%d %H:%M:%S")))

