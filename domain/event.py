from datetime import datetime
from pathlib import Path
import os


class Event:

    def __init__(self, notes, monitor_id, start_time, name, id, alarm_frame,
                 end_time):
        self.end_time = end_time
        self.alarm_frame = alarm_frame
        self.id = id
        self.name = name
        self.start_time = start_time
        self.notes = notes
        self.monitor_id = monitor_id

    def path(self):
        date_time = datetime.strptime(self.start_time, '%Y-%m-%d %H:%M:%S')
        return self.monitor_id + date_time.strftime("/%y/%m/%d/%H/%M/%S")

    def full_path(self, base_folder):
        return base_folder + '/' + self.path()

    def path_exists(self, base_folder):
        folder = Path(base_folder+self.path())
        return folder.is_dir()

    def frame_paths(self, base_folder):
        frames_paths = []
        for root, dirs, files in os.walk(self.full_path(base_folder)):
            for local_file in files:
                if local_file.endswith(".jpg"):
                    frame = os.path.join(root, local_file)
                    frames_paths.append(frame)

        frames_paths.sort()
        return frames_paths

    def __str__(self):
        return "%s %s %s" % (self.id, self.notes, self.name)



