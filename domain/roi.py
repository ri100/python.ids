from domain import region as reg


class ROI:

    def __init__(self, monitor_id, region: reg):
        self.monitor_id = monitor_id
        self.region = region
