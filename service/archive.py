import requests

from domain.detection_report import DetectionReport
from domain.event import Event


class Archive:

    def __init__(self, url):
        self.url = url

    def notify(self, event: Event, report: DetectionReport):
        try:
            data = dict()
            data['id'] = event.id
            data['action'] = 'archive'
            data['request'] = 'event'
            data['view'] = 'request'
            response = requests.post(self.url + '/index.php', data=data)
            response_data = response.json()
            return response_data['result'] == 'Ok'
        except OSError as e:
            print("Error: " + e.strerror)
            return False


if __name__ == "__main__":
    arch = Archive('http://192.168.1.123/zm')
    print(arch.notify(event=Event(None, '1', None, None, 940, 0, None),
                      report=DetectionReport(None, 0, 0, 0, 0)))
