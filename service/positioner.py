def position_to_original_image(size, x1, x2, y1, xA, yA, xB, yB):
    """

    :param x1:
    :param yB:
    :param xB:
    :param yA:
    :param xA:
    :param y1:
    :param size:
    :param x2:
    """
    scale = (x2 - x1) / size
    xx1 = int((xA * scale + x1))
    xx2 = int((xB * scale + x1))
    yy1 = int((yA * scale + y1))
    yy2 = int((yB * scale + y1))

    return xx1, yy1, xx2, yy2


def position_to_cropped_image(size, x1, x2, xA, yA, xB, yB):
    xx1 = int((xA * ((x2 - x1) / size)))
    xx2 = int((xB * ((x2 - x1) / size)))
    yy1 = int((yA * ((x2 - x1) / size)))
    yy2 = int((yB * ((x2 - x1) / size)))

    return xx1, yy1, xx2, yy2
