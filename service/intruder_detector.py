from datetime import datetime
import fcntl
import sys
from domain.detection_area import DetectionArea
from domain.detection_report import DetectionReport
from domain.event import Event
from domain.status import Status
from domain.time_pointer import TimePointer
from service.drawer import mark
from service.logger import Logger
from service.previewer import show_event_window, show_notification_window
from service.timer import millis
from service.zoneminder_api import ZoneMinderApi


class IntruderDetector:

    def __init__(self, source, base_folder, host, detection_areas: list, time_pointer_file, log: Logger):

        pid_file = '/tmp/ids.pid'
        fp = open(pid_file, 'w')
        try:
            fcntl.lockf(fp, fcntl.LOCK_EX | fcntl.LOCK_NB)
        except IOError:
            log.info('\033[0;31mAnother process running EXITING\033[0m')
            sys.exit(0)

        self.source = source
        self.time_pointer_file = time_pointer_file
        self.host = host
        self.detection_areas = detection_areas
        self.base_folder = base_folder + '/'
        self.log = log

    def detect(self):

        time_pointer = TimePointer(self.time_pointer_file)
        start = time_pointer.read()

        self.log.info("Time pointer is at %s" % start)

        zm = ZoneMinderApi(self.host)
        events = zm.get_events_for_monitor(start)

        for event in events:  # type: Event

            zm_path = event.full_path(self.base_folder)
            source_iterator = self.source.__frames__(event)

            for area in self.detection_areas:  # type: DetectionArea

                self.log.extended_info("READING area %s config for zone %s" % (area.id, event.notes), event)

                if not area.config.enabled:
                    self.log.extended_info(
                        "SKIPPED ANALYSIS of zone. It is NOT ENABLED." % area.zone_match.zone_name,
                        event)
                    continue

                status = Status(zm_path, area.algorithm.__class__.__name__ + '-' + str(area.id))

                if area.config.force_detection is False and status.exists():
                    self.log.extended_info(
                        'SKIPPED ANALYSIS %s for event %s from %s' % (area.id, event.name, event.start_time),
                        event)
                    time_pointer.write(event.start_time)
                    continue

                """
                Match roi with notes 
                """
                if not area.match_roi(event.notes, area.roi.monitor_id):
                    self.log.extended_info(
                        'SKIPPED ANALYSIS due to no zone match. %s in not %s' % (
                            event.notes, area.zone_match.zone_name),
                        event)
                    continue

                if not area.zone_match.is_it_time(datetime.strptime(event.start_time, "%Y-%m-%d %H:%M:%S")):
                    start, end = area.zone_match.period
                    self.log.extended_info(
                        "SKIPPED ANALYSIS in zone %s due to time constrain. %s-%s now is %s" % (
                            area.zone_match.zone_name, start, end, event.start_time), event)
                    continue

                if not event.path_exists(self.base_folder):
                    self.log.extended_info("SKIPPED ANALYSIS MISSING zone-minder event FOLDER %s" % zm_path, event)
                    continue

                self.log.extended_info(
                    '%s DETECTION STARTS' % area.algorithm.__class__.__name__, event)

                status.open()

                x1 = area.roi.region.x1
                x2 = area.roi.region.x2
                y1 = area.roi.region.y1

                start_time = datetime.now()
                notification_image = None
                notification_image_size = 0
                time_taken = 0
                frame_number = 0
                hit = 0
                fps = 0
                detected_object_classes_set = set()

                detector = area.algorithm

                for result in detector.__detect__(source_iterator, area.roi.region):

                    frame_number += 1

                    if len(result['objects']) > 0:
                        hit += 1

                    status.write(result, detector.image_resize, x1, x2, y1)

                    if area.config.draw_rectangle:
                        for (xA, yA, xB, yB, class_type) in result['objects']:
                            # result['image'] is full image
                            mark(result['image'], (xA, yA), (xB, yB))

                        if result['motion'] != ():
                            (mxA, myA, mxB, myB) = result['motion']
                            mark(result['image'], (mxA, myA), (mxB, myB), padding=60, color=(255, 255, 255))

                    if area.config.show_frames:
                        show_event_window(area, event, frame_number, hit, result['image'], detector.image_resize, fps)

                    for (xA, yA, xB, yB, object_cls) in result['objects']:
                        detected_object_classes_set.add(object_cls)
                        size = (xB - xA) * (yB - yA)
                        if size > notification_image_size:
                            notification_image_size = size
                            notification_image = result['image']

                    if hit > area.config.detection_stops_after:
                        break

                    time_taken = millis(start_time)
                    fps = frame_number / (time_taken / 1000)

                if area.config.show_frames:
                    show_notification_window(area, event, frame_number, hit, detector.image_resize, notification_image)

                report = DetectionReport(notification_image, hit, frame_number, fps, time_taken)

                status.close()

                msg = "%s: Monitor: %d, Area: %s, Starts %s, Zone: %s, Stats: %d/%d frames at %d fps, %f sek" % (
                    "\033[0;31mINTRUDER DETECTED\033[0m" if report.positive_frames >= area.config.alarm_after
                    else "\033[0;32mFALSE ALARM\033[0m",
                    area.roi.monitor_id,
                    area.id,
                    event.start_time,
                    event.notes,
                    report.positive_frames,
                    report.all_frames,
                    report.speed,
                    report.time_in_miliseconds / 1000
                )
                self.log.info(msg, event)

                self.log.extended_info('DETECTION ENDS for event: %s' % event.name, event)

                status_result = str(report.positive_frames > area.config.alarm_after)

                self.log.extended_info('DETECTION status=%s SAVED' % status_result, event)

                # todo add 1 sek.
                time_pointer.write(event.start_time)
                self.log.extended_info('TIME POINTER sets at %s' % event.start_time, event)

                if area.config.suppress_notifications is False:
                    if report.positive_frames >= area.config.alarm_after:
                        for trigger in area.triggers:
                            for object_class, notifications in trigger.items():
                                if object_class in detected_object_classes_set:
                                    for notification in notifications:
                                        if notification.notify(event=event, report=report):
                                            self.log.extended_info(
                                                'Notification class %s TRIGGERED SUCCESSFULLY' % trigger.__class__.__name__,
                                                event)
                                        else:
                                            self.log.extended_info('Notification class %s FAILED' % trigger.__class__.__name__,
                                                                   event)
                                else:
                                    self.log.info('Notifications DID NOT MATCHED detected object class', event)
                else:
                    self.log.extended_info('Notifications SUPPRESSED', event)
