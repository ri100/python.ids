from cv2 import cv2


def mark(image, point1, point2, thickness=3, padding=25, color=(0, 255, 0)):
    (xA, yA) = point1
    (xB, yB) = point2
    cv2.rectangle(image, point1, point2, color, 1)
    cv2.line(image, (xA, yA), (xA + padding, yA), color, thickness)
    cv2.line(image, (xA, yA), (xA, yA + padding), color, thickness)
    cv2.line(image, (xB, yB), (xB - padding, yB), color, thickness)
    cv2.line(image, (xB, yB), (xB, yB - padding), color, thickness)

    cv2.line(image, (xA, yB), (xA + padding, yB), color, thickness)
    cv2.line(image, (xA, yB), (xA, yB - padding), color, thickness)
    cv2.line(image, (xB, yA), (xB - padding, yA), color, thickness)
    cv2.line(image, (xB, yA), (xB, yA + padding), color, thickness)

    half_x = int((xB - xA) / 2)
    half_y = int((yB - yA) / 2)

    cv2.line(image, (xA + half_x, yB - 5), (xA + half_x, yB + 5), color, 1)
    cv2.line(image, (xA + half_x, yA - 5), (xA + half_x, yA + 5), color, 1)
    cv2.line(image, (xA - 5, yA + half_y), (xA + 5, yA + half_y), color, 1)
    cv2.line(image, (xB - 5, yA + half_y), (xB + 5, yA + half_y), color, 1)

