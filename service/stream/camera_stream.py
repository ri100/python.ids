from cv2 import cv2
from domain.event import Event


class CameraStream:

    def __init__(self, device = 0):
        self.video = cv2.VideoCapture(device)

    def __frames__(self, event: Event = None):
        success, image = self.video.read()
        yield image
