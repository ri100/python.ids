from cv2 import cv2
from domain.event import Event


class VideoStream:

    def __init__(self, path):
        self.video = cv2.VideoCapture(path)

    def __frames__(self, event: Event = None):
        success = True
        while success:
            success, image = self.video.read()
            yield image
