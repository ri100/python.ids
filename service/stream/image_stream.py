from cv2 import cv2
from domain.event import Event


class ImageStream:

    def __init__(self, folder):
        self.folder = folder

    def __frames__(self, event: Event):
        frames_paths = event.frame_paths(self.folder)
        for frame in frames_paths:
            image = cv2.imread(frame)

            if image is None:
                break

            yield image
