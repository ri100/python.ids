import imutils
from cv2 import cv2
import numpy as np
from imutils.object_detection import non_max_suppression

from domain.tracker import Tracker
from domain.trackers_manager import TrackersManager
from service.positioner import position_to_original_image


class HogDetectorTracking:

    def __init__(self, win_stride, padding, scale, image_resize):
        self.image_resize = image_resize
        self.scale = scale
        self.padding = padding
        self.win_stride = win_stride
        self.hog = cv2.HOGDescriptor()
        self.hog.setSVMDetector(cv2.HOGDescriptor_getDefaultPeopleDetector())
        self.trackers = TrackersManager()

    def __detect__(self, source_iterator, region):
        frame_number = 0
        for frame in source_iterator:

            frame_number += 1

            if frame is None:
                continue

            image = frame[region.y1:region.y2, region.x1:region.x2]

            # image_cropped = image.copy()

            width = min(self.image_resize, image.shape[1])
            image = imutils.resize(image, width=width)

            objects = []

            if not self.trackers.has_trackers():

                (rectangles, weights) = self.hog.detectMultiScale(image, winStride=self.win_stride,
                                                                  padding=self.padding, scale=self.scale)

                # print(rectangles, weights)
                if rectangles != ():

                    rectangles = np.array([[x, y, x + w, y + h] for (x, y, w, h) in rectangles])
                    rectangles = non_max_suppression(rectangles, probs=None, overlapThresh=0.65)

                    for (xA, yA, xB, yB) in rectangles:
                        object_position = (xA, yA, xB - xA, yB - yA)
                        tracker = Tracker(image, object_position)
                        self.trackers.add(tracker)

                        xA, yA, xB, yB = position_to_original_image(width,
                                                                    region.x1, region.x2, region.y1,
                                                                    xA, yA, xB, yB)

                        objects.append(
                            (int(xA), int(yA), int(xB), int(yB), 'person')
                        )

            tracked_object_position = []

            for idx, tracker in enumerate(self.trackers.trackers):
                ok, tracked_position = tracker.update_position(image)

                if ok:
                    (xA, yA, xB, yB) = tracked_position
                    xB = xA + xB
                    yB = yA + yB

                    xA, yA, xB, yB = position_to_original_image(width, region.x1, region.x2, region.y1, xA, yA, xB, yB)

                    tracked_object_position.append((xA, yA, xB, yB))
                    tracker.append_path((xA, yA, xB, yB))

                    is_moving = tracker.is_moving()
                    # print(idx, is_moving, tracked_position)
                    if is_moving is not None and is_moving is False:
                        self.trackers.remove(idx)
                        break
                else:
                    self.trackers.remove(idx)
                    break

            yield {'frame': frame_number, 'objects': objects, 'motion': (), 'track': tracked_object_position,
                   'image': frame}
