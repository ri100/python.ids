import numpy as np
from cv2 import cv2
from imutils.object_detection import non_max_suppression
import imutils

from service.positioner import position_to_original_image


class MotionDetector:

    def __init__(self):
        self.FirstFrame = None
        self.LastFrame = None
        self.count = 0

    def move(self, image, crop_x1, crop_y1, crop_x2, crop_y2):

        self.count += 1

        if image is None:
            return 0, 0, 0, 0

        if self.count % 4 == 0:
            self.FirstFrame = self.LastFrame

        image = image[crop_y1:crop_y2, crop_x1:crop_x2]

        width = min(400, image.shape[1])
        image = imutils.resize(image, width=width)

        gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        gray = cv2.GaussianBlur(gray, (21, 21), 0)

        self.LastFrame = gray

        # if the first frame is None, initialize it
        if self.FirstFrame is None:
            self.FirstFrame = gray
            return 0, 0, 0, 0

        # compute the absolute difference between the current frame and
        # first frame
        frame_delta = cv2.absdiff(self.FirstFrame, gray)
        thresh = cv2.threshold(frame_delta, 15, 255, cv2.THRESH_BINARY)[1]

        # dilate the thresholded image to fill in holes, then find contours
        # on thresholded image
        thresh = cv2.dilate(thresh, None, iterations=90)
        # reomoved copy
        (i, cnts, _) = cv2.findContours(thresh, cv2.RETR_EXTERNAL,
                                        cv2.CHAIN_APPROX_SIMPLE)

        # loop over the contours
        rects = []
        for c in cnts:
            # if the contour is too small, ignore it
            if cv2.contourArea(c) < 400:
                continue

            # compute the bounding box for the contour, draw it on the frame,
            # and update the text
            rects.append(cv2.boundingRect(c))

        rects = np.array([[x, y, x + w, y + h] for (x, y, w, h) in rects])
        pick = non_max_suppression(rects, probs=None, overlapThresh=0.65)

        min_x1 = 1920
        min_y1 = 1080
        max_x2 = 0
        max_y2 = 0
        for (x1, y1, x2, y2) in pick:
            max_x2 = max(max_x2, x2)
            max_y2 = max(max_y2, y2)
            min_x1 = min(min_x1, x1)
            min_y1 = min(min_y1, y1)

        if not image.any():
            return 0, 0, 0, 0

        if max_x2 - min_x1 > 0 and max_y2 - min_y1 > 0:
            min_x1, min_y1, max_x2, max_y2 = position_to_original_image(width, crop_x1, crop_x2, crop_y1, min_x1,
                                                                        min_y1, max_x2,
                                                                        max_y2)
            return min_x1, min_y1, max_x2, max_y2

        return 0, 0, 0, 0
