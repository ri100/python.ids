import imutils
from cv2 import cv2
import numpy as np
from imutils.object_detection import non_max_suppression

from domain.tracker import Tracker
from domain.trackers_manager import TrackersManager
from service.algorythm.motion_detector import MotionDetector
from service.positioner import position_to_original_image


class HogDetectorMotionCropTracking:

    def __init__(self, win_stride, padding, scale, image_resize):
        self.image_resize = image_resize
        self.scale = scale
        self.padding = padding
        self.win_stride = win_stride
        self.hog = cv2.HOGDescriptor()
        self.hog.setSVMDetector(cv2.HOGDescriptor_getDefaultPeopleDetector())
        self.motion = MotionDetector()
        self.trackers = TrackersManager()

    def __detect__(self, source_iterator, region):
        frame_number = 0
        factor = 8
        track_frame = 0

        for frame in source_iterator:

            frame_number += 1
            cropped_frame = None

            if frame is None:
                continue

            objects = []
            motion = ()
            if not self.trackers.has_trackers():

                min_x1, min_y1, max_x2, max_y2 = self.motion.move(frame, region.x1, region.y1, region.x2, region.y2)

                if max_x2 - min_x1 <= 0 or max_y2 - min_y1 <= 0:
                    yield {'frame': frame_number, 'objects': [], 'track': [], 'motion': (), 'image': frame}
                    continue

                # check if motion area is not too small
                if max_x2 - min_x1 < 250 or max_y2 - min_y1 < 250:
                    # todo make bigger
                    continue

                # crop
                image = frame[min_y1:max_y2, min_x1:max_x2]

                width = min(self.image_resize, image.shape[1])

                if width < image.shape[1]:
                    image = imutils.resize(image, width=width)

                (rectangles, weights) = self.hog.detectMultiScale(image, winStride=self.win_stride,
                                                                  padding=self.padding, scale=self.scale)

                if rectangles != ():
                    rectangles = np.array([[x, y, x + w, y + h] for (x, y, w, h) in rectangles])
                    rectangles = non_max_suppression(rectangles, probs=None, overlapThresh=0.65)

                    for (xA, yA, xB, yB) in rectangles:
                        xA, yA, xB, yB = position_to_original_image(self.image_resize,
                                                                    min_x1, max_x2, min_y1,
                                                                    xA, yA, xB, yB)

                        object_position = (
                            int(xA / factor), int(yA / factor), int((xB - xA) / factor), int((yB - yA) / factor))

                        cropped_frame = imutils.resize(frame, width=int(frame.shape[1] / factor))
                        tracker = Tracker(cropped_frame, object_position)
                        self.trackers.add(tracker)

                        track_frame = 0

                        objects.append(
                            (xA, yA, xB, yB, 'person')
                        )

                motion = (min_x1, min_y1, max_x2, max_y2)

            tracked_object_position = []

            if cropped_frame is None:
                cropped_frame = imutils.resize(frame, width=int(frame.shape[1] / factor))

                for idx, tracker in enumerate(self.trackers.trackers):
                    ok, tracked_position = tracker.update_position(cropped_frame)
                    track_frame += 1

                    if ok:
                        (xA, yA, xB, yB) = tracked_position
                        xB = xA + xB
                        yB = yA + yB

                        xA = int(xA * factor)
                        xB = int(xB * factor)
                        yA = int(yA * factor)
                        yB = int(yB * factor)

                        # xA, yA, xB, yB = position_to_original_image(width, region.x1, region.x2, region.y1, xA, yA, xB, yB)

                        tracked_object_position.append((xA, yA, xB, yB))
                        tracker.append_path((xA, yA, xB, yB))

                        is_moving = tracker.is_moving()
                        # print(idx, is_moving, tracked_position)
                        if is_moving is not None and is_moving is False:
                            self.trackers.remove(idx)
                            break
                    else:
                        self.trackers.remove(idx)
                        break

            # print(track_frame)

            yield {'frame': frame_number, 'objects': objects, 'track': tracked_object_position,
                   'motion': motion,
                   'image': frame}
