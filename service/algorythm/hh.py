from collections import deque

import imutils
from cv2 import cv2
import numpy as np
from imutils.object_detection import non_max_suppression


class HogDetectorTracking:

    def __init__(self, win_stride, padding, scale, image_resize):
        self.image_resize = image_resize
        self.scale = scale
        self.padding = padding
        self.win_stride = win_stride
        self.hog = cv2.HOGDescriptor()
        self.hog.setSVMDetector(cv2.HOGDescriptor_getDefaultPeopleDetector())
        self.trackers = []

    def __detect__(self, source_iterator, region):
        frame_number = 0
        path = deque(maxlen=6)
        for image in source_iterator:

            frame_number += 1

            if image is None:
                continue

            image = image[region.y1:region.y2, region.x1:region.x2]

            image_cropped = image.copy()

            width = min(self.image_resize, image.shape[1])
            image = imutils.resize(image, width=width)

            objects = []

            if len(path) >= path.maxlen:
                (x, y) = path[0]
                (x1, y1) = [sum(p) / len(p) for p in zip(*path)]
                print(abs(x1 - x), abs(y1 - y))
                if abs(x1 - x) < 1 and abs(y1 - y) < 1:
                    self.trackers = []
                    path.clear()

            if len(self.trackers) == 0:
                print('detect')
                (rectangles, weights) = self.hog.detectMultiScale(image, winStride=self.win_stride,
                                                                  padding=self.padding, scale=self.scale)

                if rectangles != ():

                    rectangles = np.array([[x, y, x + w, y + h] for (x, y, w, h) in rectangles])
                    rectangles = non_max_suppression(rectangles, probs=None, overlapThresh=0.65)

                    tracker_number = 0
                    for (xA, yA, xB, yB) in rectangles:
                        objects.append(
                            (xA, yA, xB, yB, 'person')
                        )

                        if tracker_number >= len(self.trackers):
                            print('init')
                            trk = cv2.TrackerKCF_create()
                            bbox = (xA, yA, xB - xA, yB - yA)
                            trk.init(image, bbox)

                            self.trackers.append(trk)

                        tracker_number += 1

            tracked_obj = []

            for tracker in self.trackers:
                if tracker is not None:
                    ok, obj = tracker.update(image)
                    (x1, y1, w, h) = obj
                    path.append((int(x1 + w / 2), int(y1 + h / 2)))
                    tracked_obj.append(obj)
                    if ok is False:
                        self.trackers = []

            yield {'frame': frame_number, 'objects': objects, 'track': tracked_obj, 'image': image_cropped}
