import imutils
from cv2 import cv2
import numpy as np
from imutils.object_detection import non_max_suppression

from service.positioner import position_to_original_image


class HogDetector:

    def __init__(self, win_stride, padding, scale, image_resize):
        self.image_resize = image_resize
        self.scale = scale
        self.padding = padding
        self.win_stride = win_stride
        self.hog = cv2.HOGDescriptor()
        self.hog.setSVMDetector(cv2.HOGDescriptor_getDefaultPeopleDetector())

    def __detect__(self, source_iterator, region):
        frame_number = 0
        for frame in source_iterator:

            frame_number += 1

            if frame is None:
                continue

            image = frame[region.y1:region.y2, region.x1:region.x2]

            width = min(self.image_resize, image.shape[1])
            image = imutils.resize(image, width=width)

            (rectangles, weights) = self.hog.detectMultiScale(image, winStride=self.win_stride,
                                                              padding=self.padding, scale=self.scale)
            objects = []
            if rectangles != ():
                rectangles = np.array([[x, y, x + w, y + h] for (x, y, w, h) in rectangles])
                rectangles = non_max_suppression(rectangles, probs=None, overlapThresh=0.65)

                for (xA, yA, xB, yB) in rectangles:
                    xA, yA, xB, yB = position_to_original_image(self.image_resize,
                                                                region.x1,
                                                                region.x2,
                                                                region.y1,
                                                                xA, yA, xB, yB)
                    objects.append(
                        (xA, yA, xB, yB, 'person')
                    )

            yield {'frame': frame_number, 'objects': objects, 'motion' : (), 'track': [] , 'image': frame}
