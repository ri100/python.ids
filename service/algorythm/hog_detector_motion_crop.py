import imutils
from cv2 import cv2
import numpy as np
from imutils.object_detection import non_max_suppression
from service.algorythm.motion_detector import MotionDetector
from service.positioner import position_to_original_image


class HogDetectorMotionCrop:

    def __init__(self, win_stride, padding, scale, image_resize):
        self.image_resize = image_resize
        self.scale = scale
        self.padding = padding
        self.win_stride = win_stride
        self.hog = cv2.HOGDescriptor()
        self.hog.setSVMDetector(cv2.HOGDescriptor_getDefaultPeopleDetector())
        self.motion = MotionDetector()

    def __detect__(self, source_iterator, region):
        frame_number = 0
        for frame in source_iterator:

            frame_number += 1

            min_x1, min_y1, max_x2, max_y2 = self.motion.move(frame, region.x1, region.y1, region.x2, region.y2)

            if max_x2 - min_x1 <= 0 or max_y2 - min_y1 <= 0:
                yield {'frame': frame_number, 'objects': [], 'track': [], 'motion': (), 'image': frame}
                continue

            # check if motion area is not too small
            if max_x2 - min_x1 < 250 or max_y2 - min_y1 < 250:
                # todo make bigger
                continue

            image = frame[min_y1:max_y2, min_x1:max_x2]

            width = min(self.image_resize, image.shape[1])

            if width < image.shape[1]:
                image = imutils.resize(image, width=width)

            (rectangles, weights) = self.hog.detectMultiScale(image, winStride=self.win_stride,
                                                              padding=self.padding, scale=self.scale)
            objects = []
            if rectangles != ():
                rectangles = np.array([[x, y, x + w, y + h] for (x, y, w, h) in rectangles])
                rectangles = non_max_suppression(rectangles, probs=None, overlapThresh=0.65)

                for (xA, yA, xB, yB) in rectangles:
                    xA, yA, xB, yB = position_to_original_image(self.image_resize,
                                                                min_x1, max_x2, min_y1,
                                                                xA, yA, xB, yB)

                    objects.append(
                        (xA, yA, xB, yB, 'person')
                    )

            yield {'frame': frame_number, 'objects': objects, 'track': [], 'motion': (min_x1, min_y1, max_x2, max_y2),
                   'image': frame}
