import requests
from domain.detection_report import DetectionReport
from domain.event import Event


class Requester:

    def __init__(self, url):
        self.url = url

    def notify(self, event: Event, report: DetectionReport):
        data = {
            "event": {
                "id": event.id,
                "monitor_id": event.monitor_id,
                "name": event.name,
                "notes": event.notes,
                "start_time": event.start_time,
                "end_time": event.end_time,
                "alarm_frame": event.alarm_frame
            },
            "report": {
                "fps": report.speed,
                "hits": report.positive_frames
            }
        }

        requests.post(url=self.url, json=data)

        return True


if __name__ == "__main__":
    r = Requester('X')
    r.notify(Event(None, None, None, None, None, None, None), DetectionReport(None, None, None, None, None))
