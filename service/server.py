import re
import socket
import threading
from time import sleep
from time import time
from datetime import datetime

import requests

ble_devices = dict()
lock = threading.Lock()


class BleDevice:
    def __init__(self, name, mac, expire_in=30):
        self.name = name
        self.time = time()
        self.mac = mac
        self.expire_in = float(expire_in)
        self.gone = None
        self.rssi = 100

    def update(self, timestamp=None, rssi=None):
        if time is None:
            self.time = time()
        else:
            timestamp = float(timestamp)
            if timestamp >= 2147483647:
                timestamp = 2147483647
            self.time = timestamp

        if rssi is not None:
            self.rssi = rssi

    def gone_for(self):
        return time() - self.time

    def is_gone(self):
        return self.time + self.expire_in < time()

    def is_mac(self, mac):
        return mac == self.mac

    def last_seen_date(self):
        return datetime.fromtimestamp(self.time).strftime('%Y-%m-%d %H:%M:%S')

    def last_seen_sec(self):
        return time() - self.time

    def set_last_state(self, gone):
        self.gone = gone

    def is_state_changed(self, gone):
        return gone != self.gone


def _log(log):
    print(datetime.now().strftime("[%Y-%m-%d %H:%M:%S] ") + log)


def server(devices_timeouts: dict):
    port = 1234
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    sock.bind(("", port))
    print("Server waiting for messages on port:", port)
    while True:
        try:
            data, addr = sock.recvfrom(1024)
            command = data.decode("utf-8")
            matched = re.match(r'^\[([A-Z]{3})\]', command, re.I)
            if matched:
                cmd = matched.group(1)
                if cmd == 'SET':
                    matched = re.match(
                        r'^\[([A-Z]{3})\] ([0-9a-f]{2}(?::[0-9a-f]{2}){5}) ([0-9]+) ([a-zA-Z0-9\-]+) ([0-9]+)',
                        command, re.I)
                    if matched:
                        mac = matched.group(2)
                        rssi = matched.group(3)
                        name = matched.group(4)
                        timestamp = matched.group(5)
                        with lock:
                            if mac in ble_devices:
                                _log(
                                    "\033[0;33m[SET]\033[0m \033[1m%s\033[0m %s:%s -> t:%s: d:%s" % (name, mac, addr, timestamp, str(
                                        float(timestamp) - time())))
                                ble_devices[mac].update(timestamp, rssi)
                            else:
                                if mac in devices_timeouts:
                                    expire_in = devices_timeouts[mac]
                                else:
                                    expire_in = 30

                                _log("\033[0;32m[NEW]\033[0m \033[1m%s\033[0m %s:%s -> exp:%s, t:%s: d:%s" % (
                                    name, mac, addr, str(expire_in), timestamp, str(
                                        float(timestamp) - time())))

                                ble_devices[mac] = BleDevice(name, mac, expire_in=expire_in)
                                ble_devices[mac].update(timestamp, rssi)
                        sock.sendto(str.encode('[OK]'), addr)
                    else:
                        sock.sendto(str.encode('[NOT-MATCHED] %s' % command), addr)

                elif cmd == 'GET':
                    matched = re.match(
                        r'^\[([A-Z]{3})\] ([0-9a-f]{2}(?::[0-9a-f]{2}){5})', command,
                        re.I)
                    if matched:
                        mac = matched.group(2)
                        _log("\033[0;34m[GET] \033[0m %s" % mac)
                        with lock:
                            if mac in ble_devices:
                                sock.sendto(str.encode('[GET] %s %s %s %s' %
                                                       (ble_devices[mac].mac, ble_devices[mac].rssi,
                                                        ble_devices[mac].name,
                                                        ble_devices[mac].time)),
                                            addr)

                    sock.sendto(str.encode('[FAIL]'), addr)

            else:
                _log('[TRASH] ' + command.rstrip())
                sock.sendto(str.encode('[FAIL]'), addr)
        except Exception as e:
            _log("\033[0;31m[ERR] \033[0m" + str(e))
            sleep(10)


class NotificationManager:

    def __init__(self, notifier=None):
        self.notifier = notifier

    @staticmethod
    def should_be_deleted(ble_device):
        return ble_device.is_gone() and ble_device.last_seen_sec() > 60 * 60 * 24  # 1 day

    def notify(self):
        while True:
            with lock:
                for ble_mac, ble_device in list(ble_devices.items()):
                    if type(ble_device) is BleDevice:

                        # remove old
                        if self.should_be_deleted(ble_device):
                            del ble_devices[ble_mac]
                            _log("\033[0;31m[DEL]\033[0m \033[1m%s\033[0m %s" % (ble_device.name, ble_device.mac))

                        if self.notifier is not None:
                            gone = ble_device.is_gone()
                            if ble_device.is_state_changed(gone):
                                ble_device.set_last_state(gone=gone)
                                if gone:
                                    self.notifier.__ble_not_present__(ble_device)
                                else:
                                    self.notifier.__ble_present__(ble_device)
            sleep(10)


class Printer:

    def __init__(self, mac_list: list = None):
        self.mac_list = mac_list

    def _notify(self, ble_device, state):
        if self.mac_list is None:
            _log("%s \033[1m%s\033[0m %s last seen at %s %f sec ago" % (
                state, ble_device.name, ble_device.mac, ble_device.last_seen_date(), ble_device.last_seen_sec()))
        elif ble_device.mac in self.mac_list:
            _log("%s \033[1m%s\033[0m %s last seen at %s %f sec ago" % (
                state, ble_device.name, ble_device.mac, ble_device.last_seen_date(), ble_device.last_seen_sec()))

    def __ble_present__(self, ble_device: BleDevice):
        self._notify(ble_device, "\033[0;32m[IN]\033[0m")

    def __ble_not_present__(self, ble_device: BleDevice):
        self._notify(ble_device, "\033[0;31m[OUT]\033[0m")


class WebNotification:

    def __init__(self, url: str, mac_list: list, verbose=False):
        self.verbose = verbose
        self.mac_list = mac_list
        self.url = url  # http://192.168.1.123:1880/monitoring

    def _request(self, state, time_span):
        body = {
            "state": state,
            "time": time_span
        }

        return requests.post(url=self.url, json=body)

    def _notify(self, ble_device, state, time_span):
        if self.mac_list is None:
            _log("%s \033[1m%s\033[0m %s last seen at %s %f sec ago" % (
                state.upper(), ble_device.name, ble_device.mac, ble_device.last_seen_date(), ble_device.last_seen_sec()))
            self._request(state, time_span)
        elif ble_device.mac in self.mac_list:
            _log("%s \033[1m%s\033[0m %s last seen at %s %f sec ago" % (
                state.upper(), ble_device.name, ble_device.mac, ble_device.last_seen_date(), ble_device.last_seen_sec()))
            self._request(state, time_span)

    def __ble_present__(self, ble_device: BleDevice):
        self._notify(ble_device, 'in', ble_device.gone_for())

    def __ble_not_present__(self, ble_device: BleDevice):
        self._notify(ble_device, 'out', ble_device.gone_for())


thread = threading.Thread(target=server, args=({"00:00:00:00:00:00": 15},))
thread.start()

ble_alarm = NotificationManager(
    notifier=WebNotification(
        url='http://192.168.1.123:1880/monitoring',  # call when device discovered
        mac_list=['cb:5a:bb:cc:cb:33']  # devices to be monitored
    )
)
ble_alarm.notify()
