import socket

# Create a UDP socket at client side

client = socket.socket(family=socket.AF_INET, type=socket.SOCK_DGRAM)
client.settimeout(2)

try:
    client.sendto(str.encode('[SET] 00:00:00:00:00:00 0 trk 02034902340234902384.787'), ("127.0.0.1", 1234))

    msg_from_server = client.recvfrom(1024)

    msg = "Message from Server {}".format(msg_from_server[0])

    print(msg)
except socket.timeout:
    print('out')


client.sendto(str.encode('[GET] 00:00:00:00:00:00'), ("127.0.0.1", 1234))

msg_from_server = client.recvfrom(1024)

msg = "Message from Server {}".format(msg_from_server[0])

print(msg)