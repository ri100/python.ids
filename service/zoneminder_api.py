import requests
import datetime
from domain.event import Event


class ZoneMinderApi:

    def __init__(self, host):
        self.host = host

    def _get_url(self, start_date, end_date, page=1):
        url = self.host + '/api/events/index/StartTime >=:' \
              + start_date + '/EndTime <=:' \
              + end_date + '.json?page=' + str(page)
        return url

    @staticmethod
    def _get_events(events_data):
        events = []
        for event_item in events_data['events']:
            event = Event(
                monitor_id=event_item['Event']['MonitorId'],
                notes=event_item['Event']['Notes'],
                start_time=event_item['Event']['StartTime'],
                name=event_item['Event']['Name'],
                id=event_item['Event']['Id'],
                alarm_frame=event_item['Event']['AlarmFrames'],
                end_time=event_item['Event']['EndTime']
            )

            events.append(event)

        return events

    def _make_request(self, start_date, end_date, page):
        try:
            return requests.post(self._get_url(start_date, end_date, page))
        except OSError as e:
            print("Error: " + e.strerror)
            return None

    def get_events_for_monitor(self, start_date):
        end_date = datetime.datetime.now()
        end_date = end_date.strftime('%Y-%m-%d %H:%M:%S')

        start_date = (datetime.datetime.strptime(start_date, '%Y-%m-%d %H:%M:%S') - datetime.timedelta(
            minutes=1)).strftime(
            '%Y-%m-%d %H:%M:%S')

        response = self._make_request(start_date, end_date, 1)
        data = response.json()
        events = self._get_events(data)

        for page in range(2, data['pagination']['pageCount'] + 1):
            response = self._make_request(start_date, end_date, page)
            events += self._get_events(response.json())

        return events


if __name__ == "__main__":
    start = '2018-04-07 22:55:21'
    events = ZoneMinderApi('http://192.168.1.123/zm').get_events_for_monitor(start)
    for ev in events:
        print(ev.name)
