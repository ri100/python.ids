from datetime import datetime

from domain.event import Event


class Logger:

    INFO = 0
    EXTENDED_INFO = 1
    DEBUG = 2

    def __init__(self, level):
        self.level = level

    @staticmethod
    def _log(event, log):
        if event is not None:
            print(datetime.now().strftime("[%Y-%m-%d %H:%M:%S] ") + event.name + ' (' + event.start_time + ') ' + log)
        else:
            print(datetime.now().strftime("[%Y-%m-%d %H:%M:%S] ") + log)

    def info(self, log, event: Event = None):
        if self.level >= self.INFO:
            self._log(event, log)

    def extended_info(self, log, event: Event = None):
        if self.level >= self.EXTENDED_INFO:
            self._log(event, log)

    def debug(self, log, event: Event = None):
        if self.level >= self.DEBUG:
            self._log(event, log)
