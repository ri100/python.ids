import uuid
from service import send_alert_via_email as alert
import os

from cv2 import cv2
from datetime import datetime
from domain.detection_report import DetectionReport
from domain.event import Event


class Mailer:

    def __init__(self, email: list):
        self.email = email
        unique_filename = str(uuid.uuid4())
        self.unique_path = "/tmp/" + unique_filename + ".jpg"

    def notify(self, event: Event, report: DetectionReport):
        cv2.imwrite(self.unique_path, report.frame)

        log = "Monitor: %s, Event: %s at %s, Zone: %s, Stats: %d/%d frames at %d fps, %f sek" % (
            event.monitor_id,
            event.name,
            event.start_time,
            event.notes,
            report.positive_frames,
            report.all_frames,
            report.speed,
            report.time_in_miliseconds / 1000
        )

        now = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        mail_body = "Monitor " + str(event.monitor_id) + ": " + event.start_time
        mail_body += "\nDetection time:" + now
        mail_body += "\nDetails: " + log + "\n"
        alert.send_alert_email(from_address='ramtamtam73@gmail.com',
                               recipients=self.email,
                               subject="Intruder detected at " + event.start_time,
                               body=mail_body, login='ramtamtam73@gmail.com',
                               password="strych12345", attachment=self.unique_path)
        os.remove(self.unique_path)

        return True
