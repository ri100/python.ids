from cv2 import cv2
from domain.detection_area import DetectionArea
from domain.event import Event


def show_event_window(area: DetectionArea, event: Event, frame_number, hit, image, image_resize, fps):
    font = cv2.FONT_HERSHEY_DUPLEX
    cv2.rectangle(image, (0, 0), (image.shape[1], 62),
                  (255, 0, 0), cv2.FILLED)
    cv2.putText(image, event.name, (1, 15), font, 0.6, (255, 255, 255), 1)
    cv2.putText(image, area.id, (1, 35), font, 0.6, (255, 255, 255), 1)
    cv2.putText(image, 'Frm: ' + str(hit) + '/' + str(frame_number) + ' FPS: '+ str(int(fps)), (0, 59), font, 0.8, (255, 255, 255), 1)
    cv2.imshow('event', image)
    cv2.waitKey(1)


def show_notification_window(area: DetectionArea, event: Event, frame_number, hit, image_resize, notification_image):
    if notification_image is not None:
        notification_image_copy = notification_image.copy()
        font = cv2.FONT_HERSHEY_DUPLEX
        if hit < area.config.alarm_after:
            cv2.rectangle(notification_image_copy, (0, 0), (notification_image_copy.shape[1], 62),
                          (0, 255, 0), cv2.FILLED)
            cv2.putText(notification_image_copy, "NEG: %s" % event.name, (1, 15), font, 0.6, (255, 255, 255), 1)
            cv2.putText(notification_image_copy, str(area.id), (1, 35), font, 0.6, (255, 255, 255), 1)
        else:
            cv2.rectangle(notification_image_copy, (0, 0), (notification_image_copy.shape[1], 62),
                          (0, 0, 255), cv2.FILLED)
            cv2.putText(notification_image_copy, "POS: %s" % event.name, (1, 15), font, 0.6, (255, 255, 255), 1)
            cv2.putText(notification_image_copy, str(area.id), (1, 35), font, 0.6, (255, 255, 255), 1)
        cv2.putText(notification_image_copy, 'Frm: ' + str(hit) + '/' + str(frame_number), (0, 59), font, 0.8,
                    (255, 255, 255), 1)
        window_id = 'notification ' + str(int(event.id) % 3)
        cv2.imshow(window_id, notification_image_copy)
