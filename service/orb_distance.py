import cv2
import matplotlib.pyplot as plt


class OrbDistance:

    def __init__(self, query_image_binary, train_image_binary):
        self.train_image_binary = train_image_binary  # trainImage
        self.query_image_binary = query_image_binary  # queryImage
        self.orb = cv2.ORB_create()

    def get_query_image(self):
        return self.orb.detectAndCompute(self.query_image_binary, None)

    def get_test_image(self):
        return self.orb.detectAndCompute(self.train_image_binary, None)

    def get_image_distance(self):
        kp1, query_image_descriptor = self.get_query_image()
        kp2, train_image_descriptor = self.get_test_image()

        # create BFMatcher object

        bf = cv2.BFMatcher(cv2.NORM_HAMMING, crossCheck=True)

        # Match descriptors.

        matches = bf.match(query_image_descriptor, train_image_descriptor)

        # Sort them in the order of their distance.

        matches = sorted(matches, key=lambda x: x.distance)

        distance = 0
        for matx in matches:
            distance += matx.distance

        return distance, matches, kp1, kp2

    def draw_matches(self, kp1, kp2, matches):
        # Draw first 10 matches.
        img3 = cv2.drawMatches(self.query_image_binary, kp1, self.train_image_binary, kp2, matches[:10], outImg=None,
                               flags=2)
        plt.imshow(img3), plt.show()
