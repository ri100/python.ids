from domain.detection_area import DetectionArea
from domain.config import Config
from domain.zonematch import ZoneMatch
from domain.region import Region
from domain.roi import ROI
from service.algorythm.hog_detector import HogDetector
from service.algorythm.hog_detector_motion_crop import HogDetectorMotionCrop
from service.archive import Archive
from service.stream.image_stream import ImageStream
from service.intruder_detector import IntruderDetector
from service.mailer import Mailer
from service.push_over import PushOver
from service.logger import Logger
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("--url", help="Zone-minder url", required=True)
parser.add_argument("--time", help="Input file that will hold time pointer (default: .ids-time)")
parser.add_argument("--preview", help="Preview video (default: No)")
parser.add_argument("--image-folder", help="Zone-minder image folder (default: /usr/share/zoneminder/www/events)")
parser.add_argument("--skip-detected", help="Global flag to skip events that are already detected (default: Yes)")
parser.add_argument("--suppress-notifications", help="Disables notifications for all detections (default: No)")
parser.add_argument("--log-level", help="0-INFO, 1-EXTENDED-INFO, 2-DEBUG (default: 0)", default=0, type=int,
                    choices=range(0, 3))

args = parser.parse_args()

args.preview = False if args.preview is None or args.preview == 'No' else (True if args.preview == 'Yes' else False)
args.url = 'http://192.168.1.123/zm' if args.url is None else args.url
args.image_folder = '/usr/share/zoneminder/www/events' if args.image_folder is None else args.image_folder
args.time = '.ids-time' if args.time is None else args.time
args.skip_detected = True if args.skip_detected is None or args.skip_detected == 'Yes' else (
    False if args.skip_detected == 'No' else True)
args.suppress_notifications = False if args.suppress_notifications is None or args.suppress_notifications == 'No' else (
    True if args.suppress_notifications == 'Yes' else False)

detection_areas = [
    DetectionArea(
        id='LivingRoom-Day',
        zone_match=ZoneMatch("All", DetectionArea.ONE_OF_MANY, ('00:00:00', '23:59:59')),
        roi=ROI(
            monitor_id=2,
            region=Region(x1=0, y1=0, x2=640, y2=480)
        ),
        config=Config(force_detection=not args.skip_detected, enabled=True,
                      suppress_notifications=args.suppress_notifications,
                      alarm_after=2,  # must be lower then anaysier_stop_after
                      detection_stops_after=25,  # stops detection after 10 positive frames
                      show_frames=args.preview
                      ),
        algorithm=HogDetectorMotionCrop(
            image_resize=250,
            win_stride=(4, 8),
            padding=(16, 16),
            scale=1.08),
        triggers=[
            {'person': [
                PushOver(
                    email='s2effy4jbj@pomail.net',
                    suppress_when_file=['/tmp/IndoorNotifications.switch'],
                    crop=None
                ),
                Archive(args.url)
            ]}
        ]
    ),
    DetectionArea(
        id='Front-Pavement-Night',
        zone_match=ZoneMatch("FrontDoor-Pavement", DetectionArea.ONE_OF_MANY, ('21:00:00', '07:00:00')),  # day/night,
        roi=ROI(
            monitor_id=1,
            region=Region(x1=800, y1=100, x2=1350, y2=900)
        ),
        config=Config(force_detection=not args.skip_detected, enabled=True,
                      suppress_notifications=args.suppress_notifications,
                      alarm_after=4,  # must be lower then anaysier_stop_after
                      detection_stops_after=15,  # stops detection after 10 positive frames
                      show_frames=args.preview
                      ),
        algorithm=HogDetectorMotionCrop(
            image_resize=250,
            win_stride=(2, 4),
            padding=(8, 8),
            scale=1
        ),
        triggers=[{'person': [PushOver(
                email='s2effy4jbj@pomail.net',
                crop=(800, 100, 1350, 900)
            ),
            Archive(args.url)]}]
        # triggers=[{'person': [Requester("http://192.168.1.123:1880/lights"), PushOver(email='s2effy4jbj@pomail.net')]}]
    ),
    DetectionArea(
        id='Front-Pavement-Day',
        zone_match=ZoneMatch("FrontDoor-Pavement", DetectionArea.ONE_OF_MANY, ('07:00:00', '21:00:00')),  # day/night,
        roi=ROI(
            monitor_id=1,
            region=Region(x1=800, y1=100, x2=1350, y2=900)
        ),
        config=Config(force_detection=not args.skip_detected, enabled=True,
                      suppress_notifications=args.suppress_notifications,
                      alarm_after=4,  # must be lower then anaysier_stop_after
                      detection_stops_after=15,  # stops detection after 10 positive frames
                      show_frames=args.preview
                      ),
        algorithm=HogDetectorMotionCrop(
            image_resize=250,
            win_stride=(4, 8),
            padding=(16, 16),
            scale=1.1),
        triggers=[
            {'person': [PushOver(
                email='s2effy4jbj@pomail.net',
                suppress_when_file=['/tmp/Risto.ble'],
                crop=(800, 100, 1350, 900)
            ),
                Archive(args.url)]}]
        # triggers=[{'person': [Requester("http://192.168.1.123:1880/lights")]}]
    ),
    DetectionArea(
        id='Car-Night',
        zone_match=ZoneMatch("Motion: Car", DetectionArea.ONE_OF_MANY, ('21:00:00', '07:00:00')),
        roi=ROI(
            monitor_id=1,
            region=Region(x1=900, y1=70, x2=1310, y2=300)
        ),
        config=Config(
            enabled=True,
            suppress_notifications=args.suppress_notifications,
            force_detection=not args.skip_detected,
            alarm_after=3, detection_stops_after=15,
            draw_rectangle=True, show_frames=args.preview
        ),
        algorithm=HogDetectorMotionCrop(
            image_resize=450,
            win_stride=(8, 8),
            padding=(16, 16),
            scale=1.07),
        triggers=[{'person': [Mailer(['risto.kowaczewski@gmail.com']), Archive(args.url)]}],
        # triggers=[{'person': [Requester("http://192.168.1.123:1880/lights")]}]
    ),
    DetectionArea(
        id='Car-Day',
        zone_match=ZoneMatch("Motion: Car", DetectionArea.ONE_OF_MANY, ('07:00:01', '20:59:59')),
        roi=ROI(
            monitor_id=1,
            region=Region(x1=900, y1=70, x2=1310, y2=300)
        ),
        config=Config(force_detection=not args.skip_detected,
                      enabled=True,
                      suppress_notifications=args.suppress_notifications,
                      alarm_after=5,
                      detection_stops_after=15,
                      draw_rectangle=True,
                      show_frames=args.preview, ),
        algorithm=HogDetectorMotionCrop(
            image_resize=400,
            win_stride=(8, 8),
            padding=(16, 16),
            scale=1.05),
        triggers=[{'person': [Mailer(['risto.kowaczewski@gmail.com']), Archive(args.url)]}]
        # triggers=[{'person': [Requester("http://192.168.1.123:1880/lights")]}]
    ),
]

logger = Logger(args.log_level)
logger.info('Process STARTED')

detector = IntruderDetector(
    source=ImageStream(args.image_folder),
    time_pointer_file=args.time,
    base_folder=args.image_folder,
    host=args.url,
    detection_areas=detection_areas,
    log=logger)
detector.detect()

logger.info('Process STOPPED')
