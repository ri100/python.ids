#!/usr/bin/env bash

sudo apt-get update
sudo apt-get install -y python3 python3-pip python-opencv

sudo -H pip3 install -r ../requirements.txt
