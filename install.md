# ZoneMinder
sudo apt-get install nfs-kernel-server
sudo nano /etc/exports
> /usr/share/zoneminder/www/events       192.168.1.154(rw,sync,no_root_squash,no_subtree_check)
sudo systemctl restart nfs-kernel-server

# IDS
sudo apt-get install nfs-common
sudo mount 192.168.1.152:/usr/share/zoneminder/www/events /mnt/zoneminder
sudo timedatectl set-timezone CET

sudo apt-get install python3-pip python-opencv
sudo -H pip3 install imutils numpy requests opencv-python request
sudo timedatectl set-timezone CET

Add to cron as root
crontab -u root -e
*/1 * * * * /usr/bin/python3 /opt/python.ids/main.py --url http://192.168.1.123/zm --time /opt/python.ids/.ids-time  >> /var/log/ids.log


