import requests
import datetime

from domain.event import Event


def get_url(start_date, end_date, page=1):
    url = 'http://192.168.1.152/zm/api/events/index/StartTime >=:' \
          + start_date + '/EndTime <=:' \
          + end_date + '.json?page=' + str(page)
    return url


def get_events(events_data):
    events = []
    for event_item in events_data['events']:
        event = Event(
            monitor_id=event_item['Event']['MonitorId'],
            notes=event_item['Event']['Notes'],
            start_time=event_item['Event']['StartTime'],
            name=event_item['Event']['Name'],
            id=event_item['Event']['Id'],
            alarm_frame=event_item['Event']['AlarmFrames'],
            end_time=event_item['Event']['EndTime']
        )

        events.append(event)

    return events


def get_events_for_monitor(start_date):
    end_date = datetime.datetime.now()
    end_date = end_date.strftime('%Y-%m-%d %H:%M:%S')

    start_date = (datetime.datetime.strptime(start_date, '%Y-%m-%d %H:%M:%S') - datetime.timedelta(minutes=1)).strftime(
        '%Y-%m-%d %H:%M:%S')

    response = requests.post(get_url(start_date, end_date, 1))
    events = get_events(response.json())
    data = response.json()

    for page in range(2, data['pagination']['pageCount'] + 1):
        response = requests.post(get_url(start_date, end_date, page))
        events += get_events(response.json())

    return events


if __name__ == "__main__":
    start = '2018-02-15 09:21:53'
    paths = get_events_for_monitor(start)
    print(len(paths))
