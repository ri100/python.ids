# pip3 install matplotlib
# sudo apt-get install python3-tk

import cv2
import matplotlib.pyplot as plt

from service.orb_distance import OrbDistance


o = OrbDistance('/home/risto/Pictures/magda_base.png', '/home/risto/Pictures/adam.png')
dis, matches, kp1, kp2 = o.get_image_distance()

o.draw_matches(kp1,kp2,matches)

print(dis)
