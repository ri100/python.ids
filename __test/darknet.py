from darknetpy.detector import Detector

detector = Detector('/home/risto/Downloads/yolo/pyyolo/darknet',
                    '/home/risto/Downloads/yolo/pyyolo/darknet/cfg/coco.data',
                    '/home/risto/Downloads/yolo/pyyolo/darknet/cfg/yolo.cfg',
                    '/home/risto/Downloads/yolo/pyyolo/tiny-yolo.weights')

results = detector.detect('<absolute-path-to>/darknet/data/dog.jpg')

print(results)