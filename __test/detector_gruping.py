from __future__ import print_function

from collections import deque
from math import sqrt
from imutils.object_detection import non_max_suppression
import numpy as np
import imutils
import cv2
from datetime import datetime

from service.orb_distance import OrbDistance

start_time = datetime.now()


# returns the elapsed milliseconds since the start of the program
def millis():
    dt = datetime.now() - start_time
    ms = (dt.days * 24 * 60 * 60 + dt.seconds) * 1000 + dt.microseconds / 1000.0
    return ms


def mark(image, point1, point2, thickness=3):
    (xA, yA) = point1
    (xB, yB) = point2
    cv2.rectangle(image, point1, point2, (40, 200, 40), 1)
    cv2.line(image, (xA, yA), (xA + 25, yA), (0, 255, 0), thickness)
    cv2.line(image, (xA, yA), (xA, yA + 25), (0, 255, 0), thickness)
    cv2.line(image, (xB, yB), (xB - 25, yB), (0, 255, 0), thickness)
    cv2.line(image, (xB, yB), (xB, yB - 25), (0, 255, 0), thickness)

    cv2.line(image, (xA, yB), (xA + 25, yB), (0, 255, 0), thickness)
    cv2.line(image, (xA, yB), (xA, yB - 25), (0, 255, 0), thickness)
    cv2.line(image, (xB, yA), (xB - 25, yA), (0, 255, 0), thickness)
    cv2.line(image, (xB, yA), (xB, yA + 25), (0, 255, 0), thickness)

    half_x = int((xB - xA) / 2)
    half_y = int((yB - yA) / 2)

    cv2.line(image, (xA + half_x, yB - 5), (xA + half_x, yB + 5), (0, 255, 0), 1)
    cv2.line(image, (xA + half_x, yA - 5), (xA + half_x, yA + 5), (0, 255, 0), 1)
    cv2.line(image, (xA - 5, yA + half_y), (xA + 5, yA + half_y), (0, 255, 0), 1)
    cv2.line(image, (xB - 5, yA + half_y), (xB + 5, yA + half_y), (0, 255, 0), 1)


def get_mark_center(point1, point2):
    (xA, yA) = point1
    (xB, yB) = point2

    half_x = int((xB - xA) / 2)
    half_y = int((yB - yA) / 2)

    return xA + half_x, yA + half_y


def get_field(point1, point2):
    (xA, yA) = point1
    (xB, yB) = point2

    return int((xB - xA)) * int((yB - yA))


def index_exists(ls, i):
    return (0 <= i < len(ls)) or (-len(ls) <= i < 0)


def detect_person(x1, y1, x2, y2, frame_threshold=1500, size=350):
    vidcap = cv2.VideoCapture('/home/risto/Downloads/Event-1187-r1-s1.avi')
    _, image = vidcap.read()

    # initialize the HOG descriptor/person detector
    hog = cv2.HOGDescriptor()
    hog.setSVMDetector(cv2.HOGDescriptor_getDefaultPeopleDetector())

    count = 0
    hit = 0
    success = True
    path = deque(maxlen=5)
    train_image_bucket = dict()
    train_image_bucket_counter = 0
    while success:
        success, image = vidcap.read()

        if image is None:
            break

        image = image[y1:y2, x1:x2]

        image = imutils.resize(image, width=size)

        result = hog.detectMultiScale(image, winStride=(8, 8),
                                      padding=(16, 16), scale=1.1)

        (rects, weights) = result

        print(weights)

        if rects != ():
            hit += 1

        if hit > frame_threshold:
            return hit

        # draw the final bounding boxes
        if rects != ():

            rects = np.array([[x, y, x + w, y + h] for (x, y, w, h) in rects])
            pick = non_max_suppression(rects, probs=None, overlapThresh=0.65)

            picked_image = None
            for (xA, yA, xB, yB) in pick:
                query_image = image[yA:yB, xA:xB]
                if not train_image_bucket:
                    train_image_bucket_counter += 1
                    train_image_bucket[train_image_bucket_counter] = query_image
                    picked_image = query_image
                else:
                    best_img_distance = 0
                    cv2.imshow('query', query_image)
                    cv2.waitKey(1)
                    for k, img in train_image_bucket.copy().items():

                        print(query_image)
                        cv2.imshow('query', query_image)
                        cv2.waitKey(1)
                        cv2.imshow('train', img)
                        cv2.waitKey(1)

                        orb_distance = OrbDistance(query_image, img)
                        img_distance, matches, _, _ = orb_distance.get_image_distance()
                        if best_img_distance < img_distance:
                            best_img_distance = img_distance
                            picked_image = img

                        # create new object
                        if best_img_distance < 10000:
                            train_image_bucket_counter += 1
                            train_image_bucket[train_image_bucket_counter] = query_image

            for k, v in train_image_bucket.items():
                cv2.imshow(str(k), v)
                cv2.waitKey(1)

            for (xA, yA, xB, yB) in pick:
                mark(image, (xA, yA), (xB, yB))

        count += 1

        cv2.imshow('frame', image)
        cv2.waitKey(10)

    print(count / (millis() / 1000))
    return hit


# side walk
print(detect_person(x1=500, y1=100, x2=1350, y2=1080, frame_threshold=100, size=400))
# print(detect_person(x1=590, y1=150, x2=1300, y2=1080, frame_threshold=100, size=350))

# car
# print(detect_person(x1=900, y1=70, x2=1310, y2=300, size=450))
