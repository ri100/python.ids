from datetime import datetime
from time import sleep

from domain.region import Region
from service.algorythm.hog_detector_motion_crop import HogDetectorMotionCrop
from service.algorythm.hog_detector_motion_crop_tracking import HogDetectorMotionCropTracking
from service.drawer import mark
import cv2

from service.stream.video_stream import VideoStream

start_time = datetime.now()


# returns the elapsed milliseconds since the start of the program
def millis():
    dt = datetime.now() - start_time
    ms = (dt.days * 24 * 60 * 60 + dt.seconds) * 1000 + dt.microseconds / 1000.0
    return ms


hog = HogDetectorMotionCropTracking(image_resize=250,
                                   win_stride=(4, 8),
                                   padding=(16, 16),
                                   scale=1.1)
hit = 0
rec = 0
count = 0
motion = 0
region = Region(x1=400, y1=100, x2=1350, y2=990)
strm = VideoStream('/home/risto/Downloads/Event-6382-r1-s1.avi').__frames__()
# region = Region(x1=0, y1=0, x2=720, y2=576)
# strm = VideoStream('rtsp://admin:STrych12345@192.168.1.241:554/1').__frames__()

for result in hog.__detect__(strm, region):
    count += 1

    if result['motion'] != ():
        motion += 1
        (xA, yA, xB, yB) = result['motion']
        mark(result['image'], (xA, yA), (xB, yB), padding=60, color=(255, 255, 255))

    for x in result['track']:
        if x != ():
            hit += 1
            (xA, yA, xB, yB) = x
            mark(result['image'], (xA, yA), (xB, yB), color=(255, 0, 100))

    for x in result['objects']:
        if x != ():
            hit += 1
            rec += 1
            (xA, yA, xB, yB, class_x) = x
            mark(result['image'], (xA, yA), (xB, yB))

    if result['image'] is not None:
        cv2.imshow('event', result['image'])
        cv2.waitKey(1)
        sleep(0.1)

print(count / (millis() / 1000))
print(count)
print(hit)
print(rec)
print(motion)
