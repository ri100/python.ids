import os

from datetime import datetime

from domain.region import Region
from service.algorythm.hog_detector_tracking import HogDetectorTracking
from service.drawer import mark
import cv2

from service.stream.video_stream import VideoStream


start_time = datetime.now()


# returns the elapsed milliseconds since the start of the program
def millis():
    dt = datetime.now() - start_time
    ms = (dt.days * 24 * 60 * 60 + dt.seconds) * 1000 + dt.microseconds / 1000.0
    return ms

class ImageStreamLocal:

    def __init__(self, folder):
        self.folder = folder

    def frame_paths(self):
        frames_paths = []
        for root, dirs, files in os.walk(self.folder):
            for local_file in files:
                if local_file.endswith(".jpg"):
                    frame = os.path.join(root, local_file)
                    frames_paths.append(frame)

        frames_paths.sort()
        return frames_paths

    def __frames__(self):
        for frame in self.frame_paths():
            image = cv2.imread(frame)

            if image is None:
                break

            yield image


hog = HogDetectorTracking(image_resize=250,
                          win_stride=(4, 8),
                          padding=(16, 16),
                          scale=1.1)

region = Region(x1=800, y1=100, x2=1350, y2=990)
# region = Region(x1=900, y1=70, x2=1310, y2=300)
# folder = '/home/risto/Pictures/frames/1/18/03/11/19/12/37'
# folder = '/home/risto/Pictures/frames/1/18/02/10/21/13'
# folder = '/home/risto/Pictures/frames/1/18/02/10/21/26'
# folder = '/home/risto/Pictures/frames/1/18/02/10/21/47'
# for result in hog.__detect__(ImageStreamLocal(folder).__frames__(), region):
count = 0
hit = 0
strm = VideoStream('/home/risto/Downloads/Event-420-r1-s1.avi').__frames__()
for result in hog.__detect__(strm, region):
    count += 1

    for x in result['track']:
        if x != ():
            hit += 1
            (xA, yA, xB, yB) = x
            mark(result['image'], (xA, yA), (xB, yB), color=(255, 0, 100))

    for x in result['objects']:
        if x != ():
            hit += 1
            (xA, yA, xB, yB, class_x) = x
            mark(result['image'], (xA, yA), (xB, yB))

    cv2.imshow('event', result['image'])
    cv2.waitKey(1)

print(count / (millis() / 1000))
print(hit)
