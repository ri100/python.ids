# camera.py
# apt install cmake
#pip3 install face_recognition
import cv2
import face_recognition


class VideoCamera(object):
    def __init__(self):
        # Using OpenCV to capture from device 0. If you have trouble capturing
        # from a webcam, comment the line below out and use a video file
        # instead.
        self.video = cv2.VideoCapture(0)
        # If you decide to use video.mp4, you must have this file in the folder
        # as the main.py.
        # self.video = cv2.VideoCapture('/home/risto/Downloads/Event-6382-r1-s1.avi')

        # Load a second sample picture and learn how to recognize it.
        biden_image = face_recognition.load_image_file("/home/risto/Pictures/risto.jpg")
        biden_face_encoding = face_recognition.face_encodings(biden_image)[0]
        # Create arrays of known face encodings and their names
        self.known_face_encodings = [
            biden_face_encoding
        ]
        self.known_face_names = [
            "risto"
        ]
        self.face_names = []

    def __del__(self):
        self.video.release()

    def get_frame(self):
        process_this_frame = True
        while True:
            success, image = self.video.read()
            # image = image[100:900, 900:1350]

            # Resize frame of video to 1/4 size for faster face recognition processing
            small_frame = cv2.resize(image, (0, 0), fx=1, fy=1)

            # Convert the image from BGR color (which OpenCV uses) to RGB color (which face_recognition uses)
            rgb_small_frame = small_frame[:, :, ::-1]
            # Only process every other frame of video to save time
            if process_this_frame:
                face_locations = face_recognition.face_locations(rgb_small_frame, model='hog')

                for (top, right, bottom, left) in face_locations:
                # print(face_locations)
                    left *= 1
                    top *= 1
                    right *= 1
                    bottom *= 1
                    cv2.rectangle(image, (left, top), (right, bottom), (0, 0, 255), 2)

            # process_this_frame = not process_this_frame

            # cv2.imshow('sml', small_frame)
            cv2.imshow('frm', image)
            cv2.waitKey(1)
        # # We are using Motion JPEG, but OpenCV defaults to capture raw images,
        # # so we must encode it into JPEG in order to correctly display the
        # # video stream.
        # ret, jpeg = cv2.imencode('.jpg', image)
        # return jpeg.tobytes()

if __name__ == "__main__":
    VideoCamera().get_frame()

