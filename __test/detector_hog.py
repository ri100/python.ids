from datetime import datetime

from domain.region import Region
from service.algorythm.hog_detector import HogDetector
from service.algorythm.hog_detector_motion_crop import HogDetectorMotionCrop
from service.drawer import mark
import cv2

from service.positioner import position_to_original_image
from service.stream.video_stream import VideoStream

start_time = datetime.now()


# returns the elapsed milliseconds since the start of the program
def millis():
    dt = datetime.now() - start_time
    ms = (dt.days * 24 * 60 * 60 + dt.seconds) * 1000 + dt.microseconds / 1000.0
    return ms


hog = HogDetectorMotionCrop(image_resize=250,
                  win_stride=(4, 8),
                  padding=(16, 16),
                  scale=1.1)
hit = 0
count = 0
region = Region(x1=800, y1=100, x2=1350, y2=990)
strm = VideoStream('/home/risto/Downloads/Event-420-r1-s1.avi').__frames__()
for result in hog.__detect__(strm, region):
    count += 1
    for x in result['objects']:
        if x != ():
            hit += 1
            (xA, yA, xB, yB, class_x) = x

            mark(result['image'], (xA, yA), (xB, yB))

    if result['image'] is not None:
        cv2.imshow('event', result['image'])
        cv2.waitKey(1)

print(count / (millis() / 1000))
print(hit)
