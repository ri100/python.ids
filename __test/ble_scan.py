import sys
import os
import requests
import struct
from ctypes import (CDLL, get_errno)
from ctypes.util import find_library
from socket import (
    socket,
    AF_BLUETOOTH,
    SOCK_RAW,
    BTPROTO_HCI,
    SOL_HCI,
    HCI_FILTER,
)

import errno
from time import sleep

# returns the elapsed milliseconds since the start of the program
from datetime import datetime


def request(url, state):
    body = {
        "state": state
    }

    requests.post(url=url, json=body)


def millis(start_time):
    dt = datetime.now() - start_time
    ms = (dt.days * 24 * 60 * 60 + dt.seconds) * 1000 + dt.microseconds / 1000.0
    return ms


def log(log):
    print(datetime.now().strftime("[%Y-%m-%d %H:%M:%S] ") + log)


if not os.geteuid() == 0:
    sys.exit("script only works as root")

btlib = find_library("bluetooth")
if not btlib:
    raise Exception(
        "Can't find required bluetooth libraries"
        " (need to install bluez)"
    )
bluez = CDLL(btlib, use_errno=True)

dev_id = bluez.hci_get_route(None)

sock = socket(AF_BLUETOOTH, SOCK_RAW, BTPROTO_HCI)
sock.bind((dev_id,))

err = bluez.hci_le_set_scan_parameters(sock.fileno(), 0, 0x10, 0x10, 0, 0, 1000)
if err < 0:
    raise Exception("Set scan parameters failed")
    # occurs when scanning is still enabled from previous call

# allows LE advertising events
hci_filter = struct.pack(
    "<IQH",
    0x00000010,
    0x4000000000000000,
    0
)
sock.setsockopt(SOL_HCI, HCI_FILTER, hci_filter)
sock.settimeout(30)

err = bluez.hci_le_set_scan_enable(
    sock.fileno(),
    1,  # 1 - turn on;  0 - turn off
    0,  # 0-filtering disabled, 1-filter out duplicates
    1000  # timeout
)
if err < 0:
    errnum = get_errno()
    raise Exception("{} {}".format(
        errno.errorcode[errnum],
        os.strerror(errnum)
    ))

start_time = datetime.now()
url = 'http://192.168.1.123:1880/monitoring'
print('Start')
flag = 'out'
while True:

    try:
        data = sock.recv(1024)
    except Exception as err:
        print(err.__str__())
        if flag == 'in':
            flag = 'out'
            request(url, flag)
            log('request out')
        sleep(10)
        continue

    print('Read: ' + str(len(data)))
    # print bluetooth address from LE Advert. packet
    mac = ':'.join("{0:02x}".format(x) for x in data[12:6:-1])

    passed_time_in_sec = millis(start_time) / 1000
    if mac == 'cb:5a:bb:cc:cb:33':
        if flag == 'out':
            flag = 'in'
            request(url, flag)
            log('request in')

        log(mac + ' exists ' + str(passed_time_in_sec))
        start_time = datetime.now()

    else:

        log(mac + ' wait ' + str(passed_time_in_sec))
        if passed_time_in_sec > 30:
            if flag == 'in':
                flag = 'out'
                request(url, flag)
                log('request out')
            log(mac + ' gone ' + str(passed_time_in_sec))
