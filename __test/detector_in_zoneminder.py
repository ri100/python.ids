# from datetime import datetime
# from domain.detection_report import DetectionReport
# from domain.detection_area import DetectionArea
# from domain.event import Event
# from domain.status import Status
# from service.algorythm.hog_detector import HogDetector
# from service.drawer import mark
# from service.previewer import show_notification_window, show_event_window
# from service.timer import millis
#
#
# def detect_person(hog,
#                   source_iterator,
#                   image_resize: int,
#                   detection_stops_after: int,
#                   win_stride: tuple,
#                   padding: tuple,
#                   scale: float,
#                   event: Event,
#                   status: Status,
#                   area: DetectionArea,
#                   draw_rectangle=False, show_frames=True) -> DetectionReport:
#     x1 = area.roi.region.x1
#     x2 = area.roi.region.x2
#     y1 = area.roi.region.y1
#     y2 = area.roi.region.y2
#
#     start_time = datetime.now()
#     notification_image = None
#     notification_image_size = 0
#     time_taken = 0
#     frame_number = 0
#     hit = 0
#     fps = 0
#
#     detector = HogDetector(win_stride=win_stride, padding=padding, scale=scale, image_resize=image_resize)
#
#     for result in detector.__detect__(source_iterator, area.roi.region):
#
#         frame_number += 1
#
#         if len(result['objects']) > 0:
#             hit += 1
#
#         status.write(result, image_resize, x1, x2, y1)
#
#         if draw_rectangle:
#             for (xA, yA, xB, yB, class_type) in result['objects']:
#                 mark(result['image'], (xA, yA), (xB, yB), padding=25)
#
#         if show_frames:
#             show_event_window(area, event, frame_number, hit, result['image'], image_resize)
#
#         for (xA, yA, xB, yB, _) in result['objects']:
#             size = (xB-xA) * (yB-yA)
#             if size > notification_image_size:
#                 notification_image_size = size
#                 notification_image = result['image']
#
#         if hit > detection_stops_after:
#             break
#
#         time_taken = millis(start_time)
#         fps = frame_number / (time_taken / 1000)
#
#     if show_frames:
#         show_notification_window(area, event, frame_number, hit, image_resize, notification_image)
#
#     return DetectionReport(notification_image, hit, frame_number, fps, time_taken)
#
#
#     # for image in source_iterator:
#     #
#     #     frame_number += 1
#     #
#     #     image = image[y1:y2, x1:x2]
#     #
#     #     width = min(image_resize, image.shape[1])
#     #     image = imutils.resize(image, width=width)
#     #
#     #     (rectangles, weights) = hog.detectMultiScale(image, winStride=win_stride,
#     #                                                  padding=padding, scale=scale)
#     #
#     #     if rectangles != ():
#     #         hit += 1
#     #
#     #         for (x, y, w, h) in rectangles:
#     #             size = w * h
#     #             if size > notification_image_size:
#     #                 notification_image_size = size
#     #                 notification_image = image
#     #
#     #         rectangles = np.array([[x, y, x + w, y + h] for (x, y, w, h) in rectangles])
#     #         pick = non_max_suppression(rectangles, probs=None, overlapThresh=0.65)
#     #
#     #         write_detection_status(frame_number, pick, status, width, x1, x2, y1)
#     #
#     #         if draw_rectangle:
#     #             for (xA, yA, xB, yB) in pick:
#     #                 mark(image, (xA, yA), (xB, yB), padding=25)
#     #
#     #     if hit > detection_stops_after:
#     #         break
#     #
#     #     if show_frames:
#     #         show_event_window(area, event, frame_number, hit, image, image_resize)
#     #
#     #     time_taken = millis(start_time)
#     #     fps = frame_number / (time_taken / 1000)
#     #
#     # if show_frames:
#     #     show_notification_window(area, event, frame_number, hit, image_resize, notification_image)
#     #
#     # return DetectionReport(notification_image, hit, frame_number, fps, time_taken)
#
#
#
