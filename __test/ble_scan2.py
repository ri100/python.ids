import sys
import os
import struct
from ctypes import (CDLL, get_errno)
from ctypes.util import find_library
import socket
from socket import (
    AF_BLUETOOTH,
    SOCK_RAW,
    BTPROTO_HCI,
    SOL_HCI,
    HCI_FILTER,
)
import errno
from datetime import datetime
from time import time, sleep


class BleScanner:

    def __init__(self, server, watch_ble=None, timeout=12, verbose=False, report_in=2):

        self.watch_ble = watch_ble
        self.server = server
        self.timeout = timeout
        self.report_in = report_in
        self.verbose = verbose
        self.ble_devices = dict()

        if not os.geteuid() == 0:
            sys.exit("script only works as root")

        self.server_client = socket.socket(family=socket.AF_INET, type=socket.SOCK_DGRAM)
        self.server_client.settimeout(2)

        bluetooth_lib = find_library("bluetooth")
        if not bluetooth_lib:
            raise Exception(
                "Can't find required bluetooth libraries"
                " (need to install bluez)"
            )

        bluez = CDLL(bluetooth_lib, use_errno=True)

        dev_id = bluez.hci_get_route(None)

        self.sock = socket.socket(AF_BLUETOOTH, SOCK_RAW, BTPROTO_HCI)
        self.sock.bind((dev_id,))

        err = bluez.hci_le_set_scan_parameters(self.sock.fileno(), 0, 0x10, 0x10, 0, 0, 1000)
        if err < 0:
            raise Exception("Set scan parameters failed")
            # occurs when scanning is still enabled from previous call

        # allows LE advertising events
        hci_filter = struct.pack(
            "<IQH",
            0x00000010,
            0x4000000000000000,
            0
        )
        self.sock.setsockopt(SOL_HCI, HCI_FILTER, hci_filter)
        self.sock.settimeout(timeout)

        err = bluez.hci_le_set_scan_enable(
            self.sock.fileno(),
            1,  # 1 - turn on;  0 - turn off
            0,  # 0-filtering disabled, 1-filter out duplicates
            1000  # timeout
        )
        if err < 0:
            errnum = get_errno()
            raise Exception("{} {}".format(
                errno.errorcode[errnum],
                os.strerror(errnum)
            ))

    def _send_to_server(self, mac, rssi=0, name='unknown') -> str:
        try:
            timestamp = int(time())
            cmd = '[SET] %s %s %s %s' % (mac, rssi, name, timestamp)
            msg = str.encode(cmd)
            self._log("\033[0;33m[SET]\033[0m %s %s \033[1m%s\033[0m %s" % (mac, rssi, name, timestamp))
            self.server_client.sendto(msg, self.server)
            msg_from_server = self.server_client.recvfrom(1024)
            return msg_from_server[0].decode("utf-8")
        except socket.timeout:
            return "\033[0;31m[ERR]\033[0m Server %s:%s timeout" % self.server
        except OSError as e:
            self._log("\033[0;31m[ERR]\033[0m " + str(e))
            sleep(30)

    def _read_data(self):

        data = self.sock.recv(1024)

        if data:
            # print bluetooth address from LE Advert. packet
            mac = ':'.join("{0:02x}".format(x) for x in data[12:6:-1])

            self._log("\033[0;32m[DIS]\033[0m " + mac)

            if self.watch_ble is None:
                response = self._send_to_server(mac)
                if response is not None:
                    self._log("\033[0;34m[RSP]\033[0m " + response)
            elif mac in self.watch_ble:
                response = self._send_to_server(mac, name=self.watch_ble[mac])
                if response is not None:
                    self._log("\033[0;34m[RSP]\033[0m " + response)

    def _log(self, log):
        if self.verbose:
            print(datetime.now().strftime("[%Y-%m-%d %H:%M:%S] ") + log)

    def scan(self):
        while True:

            try:
                self._read_data()
            except socket.timeout:
                self._log("[TIMEOUT] " + str(self.timeout) + ' sec')
            except OSError as e:
                self._log("\033[0;31m[ERR]\033[0m " + str(e))


# ['cb:5a:bb:cc:cb:33','40:0b:82:74:9d:22']
print('Start')
scanner = BleScanner(server=("192.168.1.149", 1234),
                     watch_ble={'b8:d5:0b:ac:c1:2e': 'JBL-Charge-3', 'cb:5a:bb:cc:cb:33': 'Risto-Keys'}, verbose=True)
scanner.scan()
