import pwd
import grp
import os

path = '/tmp/f.txt'
with open(path, 'a'):
    os.utime(path, None)
uid = pwd.getpwnam("nobody").pw_uid
gid = grp.getgrnam("nogroup").gr_gid

os.chown(path, uid, gid)

os.remove(path)