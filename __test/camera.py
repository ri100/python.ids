# camera.py
# apt install cmake
#pip3 install face_recognition
import cv2
import face_recognition


class VideoCamera(object):
    def __init__(self):
        # Using OpenCV to capture from device 0. If you have trouble capturing
        # from a webcam, comment the line below out and use a video file
        # instead.
        self.video = cv2.VideoCapture(0)
        # If you decide to use video.mp4, you must have this file in the folder
        # as the main.py.
        # self.video = cv2.VideoCapture('video.mp4')

        # Load a second sample picture and learn how to recognize it.
        biden_image = face_recognition.load_image_file("/home/risto/Pictures/risto.jpg")
        biden_face_encoding = face_recognition.face_encodings(biden_image)[0]
        # Create arrays of known face encodings and their names
        self.known_face_encodings = [
            biden_face_encoding
        ]
        self.known_face_names = [
            "risto"
        ]
        self.face_names = []

    def __del__(self):
        self.video.release()

    def get_frame(self):
        process_this_frame = True
        while True:
            success, image = self.video.read()

            # Resize frame of video to 1/4 size for faster face recognition processing
            small_frame = cv2.resize(image, (0, 0), fx=0.2, fy=0.2)

            # Convert the image from BGR color (which OpenCV uses) to RGB color (which face_recognition uses)
            rgb_small_frame = small_frame[:, :, ::-1]
            # Only process every other frame of video to save time
            if process_this_frame:
                face_locations = face_recognition.face_locations(rgb_small_frame)
                face_encodings = face_recognition.face_encodings(rgb_small_frame, face_locations)

                for face_encoding in face_encodings:
                # See if the face is a match for the known face(s)
                    matches = face_recognition.compare_faces(self.known_face_encodings, face_encoding)
                    name = "Unknown"
                    print(matches)
                # If a match was found in known_face_encodings, just use the first one.
                    if True in matches:
                        first_match_index = matches.index(True)
                        name = self.known_face_names[first_match_index]

                    self.face_names.append(name)

                for (top, right, bottom, left),name in zip(face_locations, self.face_names):
                # print(face_locations)
                    left *= 5
                    top *= 5
                    right *= 5
                    bottom *= 5
                    cv2.rectangle(image, (left, top), (right, bottom), (0, 0, 255), 2)
                    # Draw a label with a name below the face
                    cv2.rectangle(image, (left, bottom - 35), (right, bottom), (0, 0, 255), cv2.FILLED)
                    font = cv2.FONT_HERSHEY_DUPLEX
                    cv2.putText(image, name, (left + 6, bottom - 6), font, 1.0, (255, 255, 255), 1)

            # process_this_frame = not process_this_frame

            cv2.imshow('frm', image)
            cv2.waitKey(1)
        # # We are using Motion JPEG, but OpenCV defaults to capture raw images,
        # # so we must encode it into JPEG in order to correctly display the
        # # video stream.
        # ret, jpeg = cv2.imencode('.jpg', image)
        # return jpeg.tobytes()

if __name__ == "__main__":
    VideoCamera().get_frame()

