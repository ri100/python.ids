from __future__ import print_function

from collections import deque
from math import sqrt
from imutils.object_detection import non_max_suppression
import numpy as np
import imutils
import cv2
from datetime import datetime

start_time = datetime.now()


# returns the elapsed milliseconds since the start of the program
def millis():
    dt = datetime.now() - start_time
    ms = (dt.days * 24 * 60 * 60 + dt.seconds) * 1000 + dt.microseconds / 1000.0
    return ms


def mark(image, point1, point2, thickness=3, frame=1):
    (xA, yA) = point1
    (xB, yB) = point2
    cv2.rectangle(image, point1, point2, (40, 200, 40), frame)
    cv2.line(image, (xA, yA), (xA + 25, yA), (0, 255, 0), thickness)
    cv2.line(image, (xA, yA), (xA, yA + 25), (0, 255, 0), thickness)
    cv2.line(image, (xB, yB), (xB - 25, yB), (0, 255, 0), thickness)
    cv2.line(image, (xB, yB), (xB, yB - 25), (0, 255, 0), thickness)

    cv2.line(image, (xA, yB), (xA + 25, yB), (0, 255, 0), thickness)
    cv2.line(image, (xA, yB), (xA, yB - 25), (0, 255, 0), thickness)
    cv2.line(image, (xB, yA), (xB - 25, yA), (0, 255, 0), thickness)
    cv2.line(image, (xB, yA), (xB, yA + 25), (0, 255, 0), thickness)

    half_x = int((xB - xA) / 2)
    half_y = int((yB - yA) / 2)

    cv2.line(image, (xA + half_x, yB - 5), (xA + half_x, yB + 5), (0, 255, 0), 1)
    cv2.line(image, (xA + half_x, yA - 5), (xA + half_x, yA + 5), (0, 255, 0), 1)
    cv2.line(image, (xA - 5, yA + half_y), (xA + 5, yA + half_y), (0, 255, 0), 1)
    cv2.line(image, (xB - 5, yA + half_y), (xB + 5, yA + half_y), (0, 255, 0), 1)


def get_mark_center(point1, point2):
    (xA, yA) = point1
    (xB, yB) = point2

    half_x = int((xB - xA) / 2)
    half_y = int((yB - yA) / 2)

    return xA + half_x, yA + half_y


def get_field(point1, point2):
    (xA, yA) = point1
    (xB, yB) = point2

    return int((xB - xA)) * int((yB - yA))


def index_exists(ls, i):
    return (0 <= i < len(ls)) or (-len(ls) <= i < 0)


def position_to_original_image(size, x1, x2, y1, xA, yA, xB, yB):
    xx1 = int((xA * ((x2 - x1) / size)) + x1)
    xx2 = int((xB * ((x2 - x1) / size)) + x1)
    yy1 = int((yA * ((x2 - x1) / size)) + y1)
    yy2 = int((yB * ((x2 - x1) / size)) + y1)

    return xx1, yy1, xx2, yy2


def detect_person(x1, y1, x2, y2, frame_threshold=1500, size=350):
    vidcap = cv2.VideoCapture('/home/risto/Downloads/Event-6382-r1-s1.avi')
    _, image = vidcap.read()

    # initialize the HOG descriptor/person detector
    # hog = cv2.HOGDescriptor()
    # hog.setSVMDetector(cv2.HOGDescriptor_getDefaultPeopleDetector())

    upperBody_cascade = cv2.CascadeClassifier('haarcascade_upperbody.xml')

    count = 0
    hit = 0
    success = True
    path = deque(maxlen=5)
    while success:
        success, image = vidcap.read()

        if image is None:
            break

        orig_ig = image.copy()

        image = image[y1:y2, x1:x2]

        image = imutils.resize(image, width=size)

        rects = upperBody_cascade.detectMultiScale(image)

        # result = hog.detectMultiScale(image, winStride=(4, 8),
        #                               padding=(16, 16), scale=1.1)
        #
        # (rects, weights) = result

        # print(weights)

        if rects != ():
            hit += 1

        if hit > frame_threshold:
            return hit

        # draw the final bounding boxes
        if rects != ():

            rects = np.array([[x, y, x + w, y + h] for (x, y, w, h) in rects])
            pick = non_max_suppression(rects, probs=None, overlapThresh=0.65)

            for (xA, yA, xB, yB) in pick:
                xx1, yy1, xx2, yy2 = position_to_original_image(size, x1, x2, y1, xA, yA, xB, yB)
                mark(image, (xA, yA), (xB, yB))
                mark(orig_ig, (xx1,yy1), (xx2,yy2), frame= 2)

                # if True:
                # tracker = cv2.TrackerKCF_create()
                # # Initialize tracker with first frame and bounding box
                # ok = tracker.init(image, (xA, yA, xB, yB))
                # print(ok)
                # if ok:
                #     ok1, bbox = tracker.update(image)
                #     print(ok1)
                #     if ok1:
                #         # Tracking success
                #         p1 = (int(bbox[0]), int(bbox[1]))
                #         p2 = (int(bbox[0] + bbox[2]), int(bbox[1] + bbox[3]))
                #         cv2.rectangle(image, p1, p2, (255, 0, 0), 2, 1)

                # (cX, cY) = get_mark_center((xA, yA), (xB, yB))

                # path.append((cX, cY))

        # inward = 0
        # outward = 0
        # for i in range(0, len(path), 1):
        #     if index_exists(path, i + 1):
        #         cv2.line(image, path[i], path[i + 1], (255, 155, 0), 2)
        #         (a, b) = path[i]
        #         (c, d) = path[i + 1]
        #
        #         if d > b:
        #             inward += 1
        #         else:
        #             outward += 1
        #
        # if inward > outward:
        #     print('inward')
        # else:
        #     print('outward')

        # cv2.imwrite("frames/frame%d.jpg" % count, image)     # save frame as JPEG file
        count += 1

        cv2.imshow('frame', image)
        # cv2.imshow('oig', orig_ig)
        cv2.waitKey(1)

    print(count / (millis() / 1000))
    return hit


# side walk
print(detect_person(x1=800, y1=100, x2=1350, y2=900, frame_threshold=100, size=250))
# print(detect_person(x1=590, y1=150, x2=1300, y2=1080, frame_threshold=100, size=350))

# car
# print(detect_person(x1=900, y1=70, x2=1310, y2=300, size=450))



# import numpy as np
# import cv2
#
# img = cv2.imread('path/to/img.jpg',0)
#
# upperBody_cascade = cv2.CascadeClassifier('../path/to/haarcascade_upperbody.xml')
#
# arrUpperBody = upperBody_cascade.detectMultiScale(img)
# if arrUpperBody != ():
#         for (x,y,w,h) in arrUpperBody:
#             cv2.rectangle(img,(x,y),(x+w,y+h),(255,0,0),2)
#         print 'body found'
#
# cv2.imshow('image',img)
# cv2.waitKey(0)
cv2.destroyAllWindows()
