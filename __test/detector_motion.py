from datetime import datetime

from domain.region import Region
from service.algorythm.hog_detector_motion_crop import HogDetectorMotionCrop
from service.drawer import mark
import cv2

from service.stream.video_stream import VideoStream

start_time = datetime.now()


# returns the elapsed milliseconds since the start of the program
def millis():
    dt = datetime.now() - start_time
    ms = (dt.days * 24 * 60 * 60 + dt.seconds) * 1000 + dt.microseconds / 1000.0
    return ms


hog = HogDetectorMotionCrop(image_resize=240,
                            win_stride=(4, 8),
                            padding=(8, 16),
                            scale=1.1)
hit = 0
count = 0
region = Region(x1=0, y1=0, x2=640, y2=480)
strm = VideoStream('/home/risto/Downloads/Event-5729-r1-s1.avi').__frames__()
for result in hog.__detect__(strm, region):
    count += 1
    for x in result['objects']:
        if x != ():
            hit += 1
            (xA, yA, xB, yB, class_x) = x
            mark(result['image'], (xA, yA), (xB, yB))

    if result['motion'] != ():
        (xA, yA, xB, yB) = result['motion']
        mark(result['image'], (xA, yA), (xB, yB), padding=60, color=(255, 255, 255))

    if result['image'] is not None:
        cv2.imshow('event', result['image'])
        cv2.waitKey(100)

print(count / (millis() / 1000))
print(hit)
