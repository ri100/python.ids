import sys
import os
import requests
import struct
from time import time
from ctypes import (CDLL, get_errno)
from ctypes.util import find_library
from socket import (
    socket,
    AF_BLUETOOTH,
    SOCK_RAW,
    BTPROTO_HCI,
    SOL_HCI,
    HCI_FILTER,
)

import errno
from datetime import datetime


class BleInfo:
    def __init__(self, mac, expire_in=30):
        self.time = None
        self.update()
        self.mac = mac
        self.expire_in = float(expire_in)

    def update(self):
        self.time = time()

    def gone_for(self):
        return time() - self.time

    def is_expired(self):
        return self.time + self.expire_in < time()

    def is_mac(self, mac):
        return mac == self.mac

    def last_seen_date(self):
        return datetime.fromtimestamp(self.time).strftime('%Y-%m-%d %H:%M:%S')

    def last_seen_sec(self):
        return time() - self.time


class BleScanner:

    def __init__(self, timeout=12, verbose=False, report_in=2):

        self.timeout = timeout
        self.report_in = report_in
        self.verbose = verbose
        self.ble_devices = dict()

        if not os.geteuid() == 0:
            sys.exit("script only works as root")

        bluetooth_lib = find_library("bluetooth")
        if not bluetooth_lib:
            raise Exception(
                "Can't find required bluetooth libraries"
                " (need to install bluez)"
            )

        bluez = CDLL(bluetooth_lib, use_errno=True)

        dev_id = bluez.hci_get_route(None)

        self.sock = socket(AF_BLUETOOTH, SOCK_RAW, BTPROTO_HCI)
        self.sock.bind((dev_id,))

        err = bluez.hci_le_set_scan_parameters(self.sock.fileno(), 0, 0x10, 0x10, 0, 0, 1000)
        if err < 0:
            raise Exception("Set scan parameters failed")
            # occurs when scanning is still enabled from previous call

        # allows LE advertising events
        hci_filter = struct.pack(
            "<IQH",
            0x00000010,
            0x4000000000000000,
            0
        )
        self.sock.setsockopt(SOL_HCI, HCI_FILTER, hci_filter)
        self.sock.settimeout(timeout)

        err = bluez.hci_le_set_scan_enable(
            self.sock.fileno(),
            1,  # 1 - turn on;  0 - turn off
            0,  # 0-filtering disabled, 1-filter out duplicates
            1000  # timeout
        )
        if err < 0:
            errnum = get_errno()
            raise Exception("{} {}".format(
                errno.errorcode[errnum],
                os.strerror(errnum)
            ))

    def _read_data(self):

        data = self.sock.recv(1024)

        if data:
            # print bluetooth address from LE Advert. packet
            mac = ':'.join("{0:02x}".format(x) for x in data[12:6:-1])

            if mac in self.ble_devices:
                self._log("\033[0;32m[UPDATE] \033[0m" + mac)
                self.ble_devices[mac].update()
            else:
                self._log("\033[0;31m[NEW] \033[0m" + mac)
                self.ble_devices[mac] = BleInfo(mac)

    def _log(self, log):
        if self.verbose:
            print(datetime.now().strftime("[%Y-%m-%d %H:%M:%S] ") + log)

    def scan(self, notifier=None):

        counter = 0

        while True:

            try:
                self._read_data()
            except Exception:
                self._log("[TIMEOUT] " + str(self.timeout) + ' sec')

            counter += 1
            if counter % self.report_in == 0:
                for ble_mac, ble_device in self.ble_devices.items():
                    if type(ble_device) is BleInfo:
                        if notifier is not None:
                            if ble_device.is_expired():
                                notifier.__ble_not_present__(ble_device)
                                self._log(
                                    "\033[0;31m[GONE] \033[0m" + ble_device.mac + ' for ' + str(ble_device.gone_for()))
                            else:
                                notifier.__ble_present__(ble_device)


class Printer:

    def __init__(self, mac_list: list = None):
        self.mac_list = mac_list
        pass

    def _notify(self, ble_device, state):
        if self.mac_list is None:
            print("%s last seen at %s %f sec ago %s" % (
                ble_device.mac, ble_device.last_seen_date(), ble_device.last_seen_sec(), state))
        elif ble_device.mac in self.mac_list:
            print("%s last seen at %s %f sec ago %s" % (
                ble_device.mac, ble_device.last_seen_date(), ble_device.last_seen_sec(), state))

    def __ble_present__(self, ble_device: BleInfo):
        self._notify(ble_device, 'Visible')

    def __ble_not_present__(self, ble_device: BleInfo):
        self._notify(ble_device, 'Gone')


class PushOverNotification:

    def __init__(self, mac_list: list, verbose=False):
        self.verbose = verbose
        self.mac_list = mac_list
        self.url = 'http://192.168.1.123:1880/monitoring'
        self.state = 'out'

    def _log(self, log):
        if self.verbose:
            print(datetime.now().strftime("[%Y-%m-%d %H:%M:%S] ") + log)

    def _request_state_change(self, ble_device, state):
        if state != self.state:
            self.state = state
            self._request(state)
            self._log('[REQUEST] ' + ble_device.mac + " " + state)

    def _request(self, state):
        body = {
            "state": state
        }

        return requests.post(url=self.url, json=body)

    def _notify(self, ble_device, state):
        if self.mac_list is None:
            self._request_state_change(ble_device, state)
        elif ble_device.mac in self.mac_list:
            self._request_state_change(ble_device, state)

    def __ble_present__(self, ble_device: BleInfo):
        self._notify(ble_device, 'in')

    def __ble_not_present__(self, ble_device: BleInfo):
        self._notify(ble_device, 'out')


# ['cb:5a:bb:cc:cb:33','40:0b:82:74:9d:22']
print('Start')
scanner = BleScanner(verbose=True)
scanner.scan(
    PushOverNotification(['cb:5a:bb:cc:cb:33'], verbose=True)
)
